import ctypes
import numpy as np
from epos_layer import *

dictionary_index = {
        b'EPOS2' : {
            'gear direction': (8720, 4, 2),
            'position' : (8721, 3, ctypes.c_int32()),
            },
        b'EPOS4' : {
            'gear direction': (12306, 3, 4),
            'position': (12306, 9, ctypes.c_uint32()),
            }
        }

class _Epos(object):
    def __init__(self,device_name, nodeid, baudrate=1000000, timeout=100 ):
        self.name = VCS_GetDeviceName(self.handle)
        if not self.name == device_name.encode('utf-8'):
            raise RuntimeError('error wrong device name : {}'.format(self.name))
        self.nodeid = nodeid
        VCS_SetProtocolStackSettings(self.handle, baudrate, timeout)
        VCS_SetOperationMode(self.handle, self.nodeid, 1) # 1 is for PPM. Other value table 5-13 of epos command library
        VCS_ClearFault(self.handle, self.nodeid)
        VCS_SetEnableState(self.handle, self.nodeid)
        print('connected to {} nodeid {}'.format(self.name, self.nodeid))
        if self.get_gear_direction() == 1 : # turn trigo direction in 2T multimag, check the 1T
            self.sensor_sign = -1
        else :
            self.sensor_sign = 1
    def close(self, timeout=0):
        ret = VCS_GetOperationMode(self.handle, self.nodeid)
        if ret == 1:
            VCS_HaltPositionMovement(self.handle, self.nodeid)
        elif ret == 3:
            VCS_HaltVelocityMovement(self.handle, self.nodeid)
        elif ret == 6:
            VCS_StopHoming(self.handle, self.nodeid)
        elif ret == 7:
            VCS_StopIpmTrajectory(self.handle, self.nodeid)
        elif ret == -1:
            VCS_SetQuickStopState(self.handle, self.nodeid)
        elif ret == -2:
            VCS_SetVelocityMust(self.handle, self.nodeid, 0)
        elif ret == -3:
            VCS_SetCurrentMust(self.handle, self.nodeid, 0)
        VCS_SetDisableState(self.handle, self.nodeid)
    def get_actual_position(self):
        idex, subidex, obj_type = dictionary_index[self.name]['position']
        return VCS_GetObject(self.handle, self.nodeid, idex, subidex, obj_type)*self.sensor_sign # Position read by the SSI encorders
    def get_gear_direction(self):
        idex, subidex, bit = dictionary_index[self.name]['gear direction']
        value =  VCS_GetObject(self.handle, self.nodeid, idex, subidex, ctypes.c_uint16()) 
        return value >> bit & 1
    def target_reach(self):
        return VCS_GetMovementState(self.handle, self.nodeid)
    def move_to_multiturn_position(self, TargetPosition, relative=True):
        """ do a relative motion of the magnet (default) or go to the 
        internal position of the EPOS card if not relative.
        """
        if relative :
            VCS_MoveToPosition(self.handle, self.nodeid, TargetPosition*self.sensor_sign, 0, 1) # alway move imediately
        else :
            VCS_MoveToPosition(self.handle, self.nodeid, TargetPosition, 1, 1) # alway move imediately
    def move_to_singleturn_position(self, targetposition):
        """ reach a encoders position as fast as possible """
        actual_pos = self.get_actual_position()
        dist_forward = (targetposition-actual_pos)%32768 # 32768 is the number of increment of the encoders
        dist_backward = (actual_pos-targetposition)%32768 # 32768 is the number of increment of the encoders
        if dist_forward < dist_backward :
            self.move_to_multiturn_position(dist_forward)
        else :
            self.move_to_multiturn_position(-dist_backward)

class Epos(_Epos):
    def __init__(self, device_name, port_name, nodeid, interface_name='USB', protocol_stack_name='MAXON SERIAL V2', baudrate=1000000, timeout=100 ):
        self.handle = VCS_OpenDevice(device_name, protocol_stack_name, interface_name, port_name)
        _Epos.__init__(self, device_name, nodeid, baudrate, timeout)

class SubEpos(_Epos):
    def __init__(self, device_name, parent_handle, nodeid, protocol_stack_name='CANopen', baudrate=1000000, timeout=100 ):
        self.handle = VCS_OpenSubDevice(parent_handle, device_name, protocol_stack_name)
        _Epos.__init__(self, device_name, nodeid, baudrate, timeout)


def inc2rad(x):
    return x/2**15*(2*np.pi)

def rad2inc(x):
    return x/(2*np.pi)*2**15

class Multimag(object):
    def __init__(self, device_name, port_name, device_id, subdevice_id):
        self.epos = Epos(device_name, port_name, device_id)
        self.sub_epos = SubEpos(device_name, self.epos.handle, subdevice_id)
        self.epos_ampl = 0
        self.epos_inc0 = 0
        self.sub_epos_ampl = 0
        self.sub_epos_inc0 = 0
        self.load_parameter()
    def load_parameter(self):
        lines = [line.strip().split(' ') for line in open('parameter.txt')]
        dic = { k:np.double(v)  for k,v in lines} 
        # keep positive amplitude
        if dic['epos_ampl'] <0 :
            self.epos_ampl = -dic['epos_ampl']
            self.epos_inc0 = rad2inc((dic['epos_th0']+np.pi)%(2*np.pi))
        else :
            self.epos_ampl = dic['epos_ampl']
            self.epos_inc0 = rad2inc(dic['epos_th0']%(2*np.pi))
        if dic['sub_epos_ampl'] <0 :
            self.sub_epos_ampl = -dic['sub_epos_ampl']
            self.sub_epos_inc0 = rad2inc((dic['sub_epos_th0']+np.pi)%(2*np.pi))
        else :
            self.sub_epos_ampl = dic['sub_epos_ampl']
            self.sub_epos_inc0 = rad2inc(dic['sub_epos_th0']%(2*np.pi))
    def close(self):
        self.sub_epos.close()
        self.epos.close()
    def target_reach(self):
        return self.epos.target_reach() and self.sub_epos.target_reach()
    def get_postion(self, magnet=0):
        """ return a tuple with position of each magnet in inc, 1 is epos, 2 is sub_epos """
        if magnet == 1:
            ans = self.epos.get_actual_position()
        elif magnet == 2:
            ans = self.sub_epos.get_actual_position()
        else :
            ans = None
        return ans
    def move_to_singleturn_position(self, magnet, position):
        if magnet == 1:
            self.epos.move_to_singleturn_position(position)
        elif magnet == 2:
            self.sub_epos.move_to_singleturn_position(position)
    def move_to_multiturn_position(self, magnet, position):
        if magnet == 1:
            self.epos.move_to_multiturn_position(position)
        elif magnet == 2:
            self.sub_epos.move_to_multiturn_position(position)
    def field(self, th1, th2):
        a, b = self.epos_ampl, self.sub_epos_ampl
        Hx = a*np.cos(th1)+b*np.cos(th2)
        Hy = a*np.sin(th1)+b*np.sin(th2)
        return np.sqrt(Hx**2+Hy**2), np.arctan2(Hy, Hx)
    def inv_field(self, H0, th_H):
        """ give the position of the motors (in rad) for a given field and field angle"""""
        a, b = self.epos_ampl, self.sub_epos_ampl
        # amplitude should be positive
        H = np.abs(H0)
        # ensure we dont ask for an higher/lower field than possible
        H = max(H, np.abs(a-b))
        H = min(H, np.abs(a+b)-1e-10) # the -1e-10 fix round error
        # compute the angle
        ct = (H**2 -a**2 -b**2)/(2*a*b)
        delta_th = np.arccos(ct)
        #th0 = np.arctan2(-b*np.sqrt(1-ct**2), a+b*ct)
        if H0 < 0:
            delta_th *= -1
            th_H += np.pi
        th0 = np.arctan2(a*np.sin(delta_th/2)-b*np.sin(delta_th/2), a*np.cos(delta_th/2)+b*np.cos(delta_th/2))*2
        th1 = (th_H -th0/2) + delta_th/2
        th2 = th1 - delta_th
        return th1, th2
    def move_to_field(self, H, th_H):
        epos_th, sub_epos_th = self.inv_field(H, th_H)
        epos_th = rad2inc(epos_th)
        sub_epos_th = rad2inc(sub_epos_th)
        # corect 0 value for the encoders
        epos_th += self.epos_inc0
        sub_epos_th += self.sub_epos_inc0
        self.epos.move_to_singleturn_position(int(epos_th))
        self.sub_epos.move_to_singleturn_position(int(sub_epos_th))
    def get_field(self):
        epos_th = self.epos.get_actual_position()
        sub_epos_th = self.sub_epos.get_actual_position()
        # corect 0 value for the encoders
        epos_th -= self.epos_inc0
        sub_epos_th -= self.sub_epos_inc0
        return self.field(inc2rad(epos_th), inc2rad(sub_epos_th))


if __name__ == '__main__':
    k = Multimag('EPOS2', 'USB0', 1, 2)

