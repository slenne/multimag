from multimag import Multimag

def cmd_get_position(multimag, magnet):
    try:
        mag = int(magnet)
        val = Multimag.get_postion(multimag, mag)
        if not val is None :
            val = format(val, '05d')
    except:
        print("wrong argument")
        val = None
    return val

def cmd_get_field(multimag):
    try:
        val = Multimag.get_field(multimag)
        val = '{:06.4f} {:07.5f}'.format(*val)
    except:
        print("wrong argument")
        val = None
    return val

def cmd_move_to_field(multimag, H, H_th):
    try:
        H = float(H)
        H_th = float(H_th)
        val = Multimag.move_to_field(multimag, H, H_th)
    except:
        print("wrong argument")
        val = None
    return val

def cmd_move_to_multiturn_position(multimag, magnet, position):
    try:
        mag = int(magnet)
        pos = int(position)
        val = Multimag.move_to_multiturn_position(multimag, mag, pos)
    except:
        print("wrong argument")
        val = None
    return val

def cmd_move_to_singleturn_position(multimag, magnet, position):
    try:
        mag = int(magnet)
        pos = int(position)
        val = Multimag.move_to_singleturn_position(multimag, mag, pos)
    except:
        print("wrong argument")
        val = None
    return val

def cmd_target_reach(multimag):
    try:
        val = Multimag.target_reach(multimag)
        val = format(val, '01d')
    except:
        print("wrong argument")
        val = None
    return val

def cmd_load_parameter(multimag):
    try:
        val = Multimag.load_parameter(multimag)
    except:
        print("wrong argument")
        val = None
    return val

cmd_lib = {
        b'GetPosition' : cmd_get_position,
        b'MMP' : cmd_move_to_multiturn_position,
        b'MSP' : cmd_move_to_singleturn_position,
        b'TargetReach' : cmd_target_reach,
        b'MoveToField' : cmd_move_to_field,
        b'GetField' : cmd_get_field,
        b'LoadParameter' : cmd_load_parameter,
        }
        
def process_request(multimag, request):
    val = request.split(b' ')
    try :
        g = cmd_lib[val[0]]
    except KeyError:
        print("command {} doesn't exist".format(val[0]))
        return None
    try :
        res = g(multimag, *val[1:]) # TODO Implement a nice catch error here
    except :
        res = None
        print('argument error in {}'.format(val[0]))
        print(val)
    return res


