import ctypes
from ctypes import wintypes

#'<,'>! sed -e 's/^.*VCS/lib\.VCS/' -e 's/(/.argtypes = [/' -e 's/)/,]/' -e 's/ [^ ]*,/,/g' -e 's/char\*/ctypes\.c_wchar_p/g' -e 's/DWORD\*/wintypes\.DWORD/g' -e 's/...$//'

def convert(c):
    return ctypes.cast(ctypes.pointer(ctypes.c_char(c)), ctypes.POINTER(c_int)).contents.value

lib = ctypes.WinDLL('./../Labbiew/Resources/EposCmd.dll')

lib.VCS_OpenDevice.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_OpenDevice.restype = ctypes.c_uint32

def VCS_OpenDevice(device_name, protocol_stack_name, interface_name, port_name):
    #device_name = ctypes.c_char_p(device_name.encode('utf-8'))
    #protocol_stack_name = ctypes.c_char_p(protocol_stack_name.encode('utf-8'))
    #interface_name = ctypes.c_char_p(interface_name.encode('utf-8'))
    #port_name = ctypes.c_char_p(port_name.encode('utf-8'))
    print(device_name)
    print(protocol_stack_name)
    print(interface_name)
    print(port_name)
    errorcode = ctypes.pointer(ctypes.c_uint32(3))
    print(errorcode.contents)
    result = lib.VCS_OpenDevice(device_name.encode('utf-8'), protocol_stack_name.encode('utf-8'), interface_name.encode('utf-8'), port_name.encode('utf-8'), errorcode)
    print(errorcode.contents)
    print(result)
    return result,errorcode


lib.VCS_GetProtocolStackSettings.argtypes = [wintypes.HANDLE, wintypes.DWORD, wintypes.DWORD, wintypes.DWORD]
lib.VCS_GetProtocolStackSettings.restype = wintypes.BOOL

def VCS_GetProtocolStackSettings(KeyHandle):
    baudrate = wintypes.DWORD()
    timeout = wintypes.DWORD()
    errorcode = wintypes.DWORD()
    result = lib.VCS_GetProtocolStackSettings(KeyHandle,ctypes.byref(baudrate),ctypes.byref(timeout),ctypes.byref(errorcode))
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.value))
    return baudrate.value, timeout.value


class EPOS(object):
    def __init__(self, device_name, port_name, nodeid,interface_name='USB', protocol_stack_name='MAXON SERIAL V2', bauderate=1000000, timeout=100 ):
        self.nodeid = nodeid
        self.handle = VCS_OpenDevice(device_name, protocol_stack_name, interface_name, port_name)
        print(self.handle)
        #need to catch error properly. Probably wrap all eposdll command in an object and after heritage.
        #eposdll.VCS_SetProtocolStackSettings(self.handle, bauderate, timeout, self.error_buf)
        #eposdll.VCS_SetOperationMode(self.handle, self.nodeid, 1) # 1 is for PPM. Other value table 5-13 of epos command library
        #eposdll.VCS_ClearFault(self.handle, self.nodeid)
    def close(self, timeout=0):
        ret = ctypes.c_int8()
        eposdll.VCS_GetOperationMode(self.handle, self.nodeid, ret, self.error_buf)
        if ret.value == 1:
            eposdll.VCS_HaltPositionMovement(self.handle, self.nodeid, self.error_buf)
        elif ret.value == 3:
            eposdll.VCS_HaltVelocityMovement(self.handle, self.nodeid, self.error_buf)
        elif ret.value == 6:
            eposdll.VCS_StopHoming(self.handle, self.nodeid, self.error_buf)
        elif ret.value == 7:
            eposdll.VCS_StopIpmTrajectory(self.handle, self.nodeid, self.error_buf)
        elif ret.value == -1:
            eposdll.VCS_SetQuickStopState(self.handle, self.nodeid, self.error_buf)
        elif ret.value == -2:
            eposdll.VCS_SetVelocityMust(self.handle, self.nodeid, self.error_buf, 0)
        elif ret.value == -3:
            eposdll.VCS_SetCurrentMust(self.handle, self.nodeid, self.error_buf, 0)
        eposdll.VCS_SetDisableState(self.handle, self.nodeid, self.error_buf)
        
if __name__ == '__main__':
    k = EPOS('EPOS4', 'USB0', 1)


print('ok')
