# python3
# This file correspond to a wrapper of the EposCmd libray function It use the
# ctypes libray to load and control the C library and adapt the function to a
# more pythonic style.
# To more detail on the function look at the epos libray documentation
import ctypes


lib = ctypes.WinDLL('./EposCmd.dll')

lib.VCS_OpenDevice.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_OpenDevice.restype = ctypes.c_int32

def VCS_OpenDevice(device_name, protocol_stack_name, interface_name, port_name):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_OpenDevice(device_name.encode('utf-8'), protocol_stack_name.encode('utf-8'), interface_name.encode('utf-8'), port_name.encode('utf-8'), errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))
    return result

lib.VCS_OpenSubDevice.argtypes = [ctypes.c_int32,  ctypes.c_char_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_OpenSubDevice.restype = ctypes.c_uint32

def VCS_OpenSubDevice(KeyHandle, device_name, protocol_stack_name):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_OpenSubDevice(KeyHandle, device_name.encode('utf-8'), protocol_stack_name.encode('utf-8'), errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))
    return result

lib.VCS_SetProtocolStackSettings.argtypes = [ctypes.c_int32, ctypes.c_uint32, ctypes.c_uint32, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_SetProtocolStackSettings.restype = ctypes.c_uint32

def VCS_SetProtocolStackSettings(KeyHandle, baudrate, timeout):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_SetProtocolStackSettings(KeyHandle, baudrate, timeout, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_SetOperationMode.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.c_int8, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_SetOperationMode.restype = ctypes.c_uint32

def VCS_SetOperationMode(KeyHandle, nodeid, value):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_SetOperationMode(KeyHandle, nodeid, value, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_GetOperationMode.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_int8), ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_GetOperationMode.restype = ctypes.c_uint32

def VCS_GetOperationMode(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    value = ctypes.pointer(ctypes.c_int8())
    result = lib.VCS_GetOperationMode(KeyHandle, nodeid, value, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))
    return value.contents.value

lib.VCS_ClearFault.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_ClearFault.restype = ctypes.c_uint32

def VCS_ClearFault(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_ClearFault(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_HaltPositionMovement.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_HaltPositionMovement.restype = ctypes.c_uint32

def VCS_HaltPositionMovement(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_HaltPositionMovement(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_HaltVelocityMovement.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_HaltVelocityMovement.restype = ctypes.c_uint32

def VCS_HaltVelocityMovement(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_HaltVelocityMovement(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_StopHoming.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_StopHoming.restype = ctypes.c_uint32

def VCS_StopHoming(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_StopHoming(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_StopIpmTrajectory.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_StopIpmTrajectory.restype = ctypes.c_uint32

def VCS_StopIpmTrajectory(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_StopIpmTrajectory(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_SetQuickStopState.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_SetQuickStopState.restype = ctypes.c_uint32

def VCS_SetQuickStopState(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_SetQuickStopState(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_SetVelocityMust.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.c_int32, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_SetVelocityMust.restype = ctypes.c_uint32

def VCS_SetVelocityMust(KeyHandle, nodeid, value):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_SetVelocityMust(KeyHandle, nodeid, value, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_SetCurrentMust.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.c_int16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_SetCurrentMust.restype = ctypes.c_uint32

def VCS_SetCurrentMust(KeyHandle, nodeid, value):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_SetCurrentMust(KeyHandle, nodeid, value, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_SetEnableState.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_SetEnableState.restype = ctypes.c_uint32

def VCS_SetEnableState(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_SetEnableState(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_SetDisableState.argtypes = [ctypes.c_int32, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_SetDisableState.restype = ctypes.c_uint32

def VCS_SetDisableState(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_SetDisableState(KeyHandle, nodeid, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

lib.VCS_GetDeviceName.argtypes = [ctypes.c_int32,  ctypes.c_char_p, ctypes.c_uint16, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_GetDeviceName.restype = ctypes.c_uint32

def VCS_GetDeviceName(KeyHandle):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    device_name = ctypes.create_string_buffer(100)
    result = lib.VCS_GetDeviceName(KeyHandle, device_name, 100, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))
    return device_name.value

lib.VCS_GetPositionIs.argtypes = [ctypes.c_int32,  ctypes.c_uint16, ctypes.POINTER(ctypes.c_int32), ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_GetPositionIs.restype = ctypes.c_uint32

def VCS_GetPositionIs(KeyHandle, nodeid):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    value = ctypes.pointer(ctypes.c_int32())
    result = lib.VCS_GetPositionIs(KeyHandle, nodeid, value, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))
    return value.contents.value

#Configuration_DllExport BOOL __stdcall VCS_GetObject(HANDLE KeyHandle, WORD NodeId, WORD ObjectIndex, BYTE ObjectSubIndex, void* pData, DWORD NbOfBytesToRead, DWORD* pNbOfBytesRead, DWORD* pErrorCode);
lib.VCS_GetObject.argtypes = [ctypes.c_int32,  ctypes.c_uint16, ctypes.c_uint16, ctypes.c_uint8, ctypes.c_voidp, ctypes.c_uint32, ctypes.POINTER(ctypes.c_uint32), ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_GetObject.restype = ctypes.c_uint32

def VCS_GetObject(KeyHandle, nodeid, index, subindex, c_type):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    value = ctypes.pointer(c_type)
    check_read = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_GetObject(KeyHandle, nodeid, index, subindex, value, ctypes.sizeof(c_type), check_read, errorcode)
    if not result and not check_read.contents.value == ctypes.sizeof(c_type):
        raise RuntimeError('error code = 100000B')
    return value.contents.value

#int32_t VCS_MoveToPosition(uint32_t KeyHandle, uint16_t NodeId, int32_t TargetPosition, int32_t Absolute, int32_t Immediately, uint32_t *pErrorCode);
lib.VCS_MoveToPosition.argtypes = [ctypes.c_int32,  ctypes.c_uint16, ctypes.c_int32, ctypes.c_int32, ctypes.c_int32, ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_MoveToPosition.restype = ctypes.c_uint32

def VCS_MoveToPosition(KeyHandle, NodeId, TargetPosition, Absolute, Immediately):
    errorcode = ctypes.pointer(ctypes.c_uint32())
    result = lib.VCS_MoveToPosition(KeyHandle, NodeId, TargetPosition, Absolute, Immediately, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))

#int32_t VCS_GetMovementState(uint32_t KeyHandle, uint16_t NodeId, int32_t *pTargetReached, uint32_t *pErrorCode);
lib.VCS_GetMovementState.argtypes = [ctypes.c_int32,  ctypes.c_uint16, ctypes.POINTER(ctypes.c_int32), ctypes.POINTER(ctypes.c_uint32)]
lib.VCS_GetMovementState.restype = ctypes.c_uint32

def VCS_GetMovementState(KeyHandle, NodeId):
    """ return True if the target is reach"""
    errorcode = ctypes.pointer(ctypes.c_uint32())
    value = ctypes.pointer(ctypes.c_int32())
    result = lib.VCS_GetMovementState(KeyHandle, NodeId, value, errorcode)
    if not result:
        raise RuntimeError('error code = {}'.format(errorcode.contents.value))
    return not value.contents.value == 0
