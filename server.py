# python3
# tcp server loading the multimag and managing the connection
import selectors
import socket
import time
from multimag import Multimag
from command_library import process_request

sel = selectors.DefaultSelector()

def accept(sock, mask):
    """ create new connection """
    conn, addr = sock.accept()
    print('accepted connection from', addr)
    conn.setblocking(False)
    sel.register(conn, selectors.EVENT_READ, data=Read())


class Read:
    """ A read object is use to store all the data stream and reconise when a
    new command is present """
    def __init__(self):
        self.dat = b''
    def __call__(self, conn, mask):
        data = conn.recv(1024)
        if data:
            self.dat += data
            if b'\r\n' in self.dat: # the command end by a return caractère
                val = self.dat.split(b'\r\n')
                self.dat = val[-1]
                return conn, val[:-1]
        else:
            print('closing')
            sel.unregister(conn)
            conn.close()

def send(conn, msg):
    """ send the answer in 2 step first send a 4 bytes containging the
    length of the mesg and the second containging the mesg itself """
    msg_lens = format(len(msg), '04d').encode('utf-8')
    try :
        conn.send(msg_lens)
        conn.send(msg)
    except ConnectionAbortedError :
        print('closing after connection error')
        sel.unregister(conn)
        conn.close()


sock = socket.socket()
sock.bind(('localhost', 65432)) # host and port of the connection
sock.listen(100)
sock.setblocking(False)
sel.register(sock, selectors.EVENT_READ, accept)


try :
    k = Multimag('EPOS2', 'USB0', 1, 2) # multimag object
    while True: # look at the selectors documentation to understand the next step
        events = sel.select(0)
        for key, mask in events:
            calf = key.data
            stream = calf(key.fileobj, mask)
            if stream:
                conn, val = stream
                for v in val:
                    print(v)
                    res = process_request(k, v) # send the request to the multimag itself
                    if res: # if the Multimag send a answer just send it with the tcp socket
                        print('echoing', res)
                        send(conn, res.encode('utf-8'))
        time.sleep(0.05)
except KeyboardInterrupt :
    print('close')
    k.close()
    import sys
    sys.exit(0)
